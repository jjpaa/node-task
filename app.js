const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two numbers together
 * @param {Number} a 
 * @param {Number} b
 * @returns {Number}
 */
const add = (a,b) => {
    return a + b;
}

/**
 * This function is made to just test things
 * @param {Input Value} a 
 * @param {string} b 
 * @param {*} c
 * @returns {string}
 */
const test = (a,b) => {
  const c = 0;
  return c
}


/** Path to add function */
app.get('/add/', (req, res) => {
    const x = add(1,2)
    res.send(`Sum: ${x}`);
  })

/** Welcome page */
app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})