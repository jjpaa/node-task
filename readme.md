# Node Task

This project contains node 
hello-world

## Installation steps

1. Copy the repository
2. Install dependencies
3. Start the app

## Terminal commands

Code block:

```sh
git clone ""
cd ./node-taks
npm i
```
Start cmd: `npm run start`

## App should work

![alt text for test image](https://upload.wikimedia.org/wikipedia/en/6/63/Feels_good_man.jpg "text")